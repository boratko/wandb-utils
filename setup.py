from setuptools import setup

setup(
    name="wandb_utils",
    version="0.1",
    python_requires=">=3.7",
    description="Weights and Biases Utils",
    packages=["wandb_utils"],
    package_dir={"": "src"},
    install_requires=["wandb", "toml"],
    zip_safe=False,
)
