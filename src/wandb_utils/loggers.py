import wandb
from pytorch_utils.loggers import Logger

__all__ = [
    "WandBLogger",
]


class WandBLogger(Logger):
    def commit(self) -> None:
        """
        If there are values to log, log them.
        """
        if self.has_collected_data:
            wandb.log(self._log_dict)
            self.clear()
