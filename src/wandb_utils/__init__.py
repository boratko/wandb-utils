import pickle
from pathlib import Path
from typing import *
import hashlib
import base64

import toml
import wandb
from pytorch_utils.file_loading import find_file

__all__ = [
    "download_from_wandb",
    "select_best_run",
    "load_config",
]


def download_from_wandb(
    wandb_id: str, output_dir: Union[Path, str], metric: Optional[str] = None
) -> None:
    """
    Download a model from Weights & Biases
    :param wandb_id: Run or sweep identifier (eg "[entity]/[project]/[run_id]" or "[entity]/[project]/sweeps/[sweep_id]")
    :param output_dir: Directory to download model to
    :param metric: Name of metric to inspect (if `wandb_id` is a sweep)
    """
    output_dir = Path(output_dir).expanduser()
    output_dir.mkdir(parents=True, exist_ok=True)
    if wandb_id.find("sweeps") != -1:
        if metric is None:
            # TODO: See if we can look up the metric name that the sweep is optimizing for
            raise ValueError(
                "Attempted to download the best run from a sweep but did not pass a metric name"
            )
        run = select_best_run(wandb_id, metric)
    else:
        api = wandb.Api()
        run = api.run(wandb_id)
    print(f"Downloading metadata to {output_dir}...", flush=True, end="")
    with open(output_dir / "wandb_run_info.toml", "w") as f:
        toml.dump(
            {
                "id": run.id,
                "name": run.name,
                "notes": run.notes,
                "tags": run.tags,
                "sweep": run.sweep,
                "path": run.path,
                "state": run.state,
                "project": run.project,
                "url": run.url,
                "entity": run.entity,
                "accessed_with_wandb_id": wandb_id,
            },
            f,
        )
    with open(output_dir / "summary.toml", "w") as f:
        toml.dump(dict(run.summary), f)
    with open(output_dir / "config.toml", "w") as f:
        toml.dump(run.config, f)
    print("done!")
    print(f"Downloading history to {output_dir}/history.pkl.xz...", end="", flush=True)
    run.history().to_pickle(output_dir / "history.pkl.xz")
    print("done!")
    files = run.files()
    print(f"Found {len(files)} files.")
    for file in run.files():
        download_to_file = output_dir / file.name
        if download_to_file.exists():
            print(
                f"File {download_to_file} already exists on disk. Checking hash...",
                flush=True,
                end="",
            )
            with download_to_file.open("rb") as file_on_disk:
                file_on_disk_hash = base64.b64encode(
                    hashlib.md5(file_on_disk.read()).digest()
                ).decode("utf-8")
                if file_on_disk_hash == file.md5:
                    print(f"match! Skipping.")
                    continue
                else:
                    print("different! Redownloading!")
        print(f"Downloading {file}...", flush=True, end="")
        file.download(replace=True, root=output_dir)
        print("done!")


def select_best_run(sweep_id: str, metric: str) -> wandb.apis.public.Run:
    """
    Select the best run from a sweep based on the specified metric.
    :param sweep_id: Sweep identifier (eg. "[entity]/[project]/sweeps/[sweep_id]")
    :param metric: Name of metric to inspect
    :return: run object which has the maximum value for metric within this sweep
    """
    # TODO: generalize this for min/max metrics
    api = wandb.Api()
    sweep = api.sweep(sweep_id)
    runs = sorted(sweep.runs, key=lambda run: run.summary.get(metric, 0), reverse=True,)
    best_run = runs[0]
    metric_value = best_run.summary.get(metric, 0)
    print(f"Best run {best_run.name} with {metric} = {metric_value}")
    return best_run


def load_config(filename_or_dir: Union[Path, str]) -> Dict[str, Any]:
    """
    Load the config for a given run.
    :param filename_or_dir: directory where config file can be found, or explicit filename of config
    :return: config dictionary
    """
    filename_or_dir = Path(filename_or_dir).expanduser()
    dir = filename_or_dir if filename_or_dir.is_dir() else filename_or_dir.parent
    override_filename = filename_or_dir.name if filename_or_dir.is_file() else None
    filepath = find_file(
        data_dir=dir,
        default_filename="config",
        override_filename=override_filename,
        suffixes=(".toml", ".pkl"),
        must_exist=True,
    )
    if ".toml" in filepath.suffixes:
        config = toml.load(filepath.open("r"))
    if ".pkl" in filepath.suffixes:
        config = pickle.load(filepath.open("rb"))
    return config
